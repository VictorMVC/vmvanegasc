/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Sistemas-11
 */
public class EmpleadoDAO implements Serializable{
    
    public String PNombre;
    public String SNombre;
    public String PApellido;
    public String SApellido;
    public int Edad;
    public String Oficio;
    public int Experiencia;
    public Date nacimiento;
    public Date contrato;

    public EmpleadoDAO() {
    }

    public EmpleadoDAO(String PNombre, String SNombre, String PApellido, String SApellido, int Edad, String Oficio, int Experiencia, Date nacimiento, Date contrato) {
        this.PNombre = PNombre;
        this.SNombre = SNombre;
        this.PApellido = PApellido;
        this.SApellido = SApellido;
        this.Edad = Edad;
        this.Oficio = Oficio;
        this.Experiencia = Experiencia;
        this.nacimiento = nacimiento;
        this.contrato = contrato;
    }

    public String getPNombre() {
        return PNombre;
    }

    public void setPNombre(String PNombre) {
        this.PNombre = PNombre;
    }

    public String getSNombre() {
        return SNombre;
    }

    public void setSNombre(String SNombre) {
        this.SNombre = SNombre;
    }

    public String getPApellido() {
        return PApellido;
    }

    public void setPApellido(String PApellido) {
        this.PApellido = PApellido;
    }

    public String getSApellido() {
        return SApellido;
    }

    public void setSApellido(String SApellido) {
        this.SApellido = SApellido;
    }

    public int getEdad() {
        return Edad;
    }

    public void setEdad(int Edad) {
        this.Edad = Edad;
    }

    public String getOficio() {
        return Oficio;
    }

    public void setOficio(String Oficio) {
        this.Oficio = Oficio;
    }

    public int getExperiencia() {
        return Experiencia;
    }

    public void setExperiencia(int Experiencia) {
        this.Experiencia = Experiencia;
    }

    public Date getNacimiento() {
        return nacimiento;
    }

    public void setNacimiento(Date nacimiento) {
        this.nacimiento = nacimiento;
    }

    public Date getContrato() {
        return contrato;
    }

    public void setContrato(Date contrato) {
        this.contrato = contrato;
    }
    
    
    
}
