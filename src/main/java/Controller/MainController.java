/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Models.CustomFile;
import Models.EmpleadoDAO;
import com.is2t1.examen2t1.viewa.EmployeeFrame;
import com.is2t1.examen2t1.viewa.MainFrame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author Sistemas-11
 */
public class MainController implements ActionListener {
    
    static int windowsCount = 0;
    private MainFrame frame;
    private JFileChooser fc; 
    public MainController(){
        frame = new MainFrame();
        fc = new JFileChooser();
    }
    public MainController(MainFrame f){
        frame = f;
        fc = new JFileChooser();
    }
    
    


    @Override
    public void actionPerformed(ActionEvent e) {
        switch(e.getActionCommand())
        {
            case "new":
                showNewForm();
                break;
            case "open":
                showOpenFileDialog();
                break;
            case "save":
                showSaveFileDialog();
                break;
        }
    }
    
     private void showFileInForm(CustomFile customfile)
    {
            EmployeeFrame n = new EmployeeFrame();
            n.setVisible(true);
            
    }
    
     private void showNewForm()
    {
            windowsCount++;
            String title = "Sin título " + String.valueOf(windowsCount);
            EmployeeFrame n = new EmployeeFrame(title);
            n.setVisible(true);
            
    }
     
     private File showOpenFileDialog(){
        File file = null;
        if (fc.showOpenDialog(frame)==JFileChooser.APPROVE_OPTION){
            file = fc.getSelectedFile();
            if(file!=null){
                try {
                    readFile(file);
                } catch (IOException ex) {
                    Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            else{
               JOptionPane.showMessageDialog(frame, "No se seleccionó archivo");
            }
        }
        return file;
    }
     
      private void showSaveFileDialog(){
       File file;
       if (fc.showSaveDialog(frame)==JFileChooser.APPROVE_OPTION){
            file = fc.getSelectedFile();
            if(file!=null){
                try {                    
                    writeFile(file, getDataFromForm());
                } catch (IOException ex) {
                    Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            else{
               JOptionPane.showMessageDialog(frame, "No se seleccionó archivo", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }        
    }
     
    private void readFile(File file) throws FileNotFoundException, IOException, ClassNotFoundException {
        
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));
            CustomFile myCustomFile = (CustomFile) ois.readObject();
            myCustomFile.setFileName(file.getName());
            //JOptionPane.showMessageDialog(frame, myCustomFile.getContent());
            showFileInForm(myCustomFile);
        
    }
    
    
    private CustomFile getDataFromForm(){
        JInternalFrame [] jiframes = frame.getAllFrames();
        CustomFile cf = null;        
        for(int i=0; i<jiframes.length; i++){
            if(jiframes[i].isSelected())
            {
                if(jiframes[i].getClass().getName().equals(EmployeeFrame.class.getName())){
                    cf = ((EmployeeFrame)jiframes[i]).getData();
                }
            }
        }        
        return cf;
    }
    
    
    private void writeFile(File file, CustomFile myFile) throws IOException {      
        try {
            ObjectOutputStream w = new ObjectOutputStream(new FileOutputStream(file));
            w.writeObject(myFile);
            w.flush();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
